-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.19-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para conecta2company
DROP DATABASE IF EXISTS `conecta2company`;
CREATE DATABASE IF NOT EXISTS `conecta2company` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `conecta2company`;

-- Volcando estructura para tabla conecta2company.categoria
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `abreviatura` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `conatdor` int(11) NOT NULL,
  `productosasociados` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.clientes
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `documento` int(11) NOT NULL,
  `email` text COLLATE utf8_spanish_ci NOT NULL,
  `telefono` text COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `compras` int(11) NOT NULL,
  `ultima_compra` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fechaalta` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.contador
DROP TABLE IF EXISTS `contador`;
CREATE TABLE IF NOT EXISTS `contador` (
  `venta` int(11) DEFAULT NULL,
  `sucursal` int(11) DEFAULT NULL,
  `usuario` int(11) DEFAULT NULL,
  `colabrador` int(11) DEFAULT NULL,
  `producto` int(11) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.contrato
DROP TABLE IF EXISTS `contrato`;
CREATE TABLE IF NOT EXISTS `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) DEFAULT NULL,
  `total` double(14,0) DEFAULT NULL,
  `fechaalta` datetime DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.cortecaja
DROP TABLE IF EXISTS `cortecaja`;
CREATE TABLE IF NOT EXISTS `cortecaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) DEFAULT NULL,
  `total` double(14,0) DEFAULT NULL,
  `fechaalta` datetime DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.diasemana
DROP TABLE IF EXISTS `diasemana`;
CREATE TABLE IF NOT EXISTS `diasemana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `nombremay` varchar(255) DEFAULT NULL,
  `nombremin` varchar(255) DEFAULT NULL,
  `abrevmin` varchar(255) DEFAULT NULL,
  `abrevmay` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.domicilio
DROP TABLE IF EXISTS `domicilio`;
CREATE TABLE IF NOT EXISTS `domicilio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calle` text COLLATE utf8_spanish_ci NOT NULL,
  `numext` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `numint` text COLLATE utf8_spanish_ci NOT NULL,
  `colonia` int(11) DEFAULT NULL,
  `codigopostal` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `delegacion` text COLLATE utf8_spanish_ci NOT NULL,
  `municipio` text COLLATE utf8_spanish_ci NOT NULL,
  `coordenadas` date NOT NULL,
  `entrecalle1` int(11) NOT NULL,
  `entrecalle2` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `referenciaubicacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaalta` timestamp NULL DEFAULT NULL,
  `fechaedicion` datetime NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `estatus_id` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.estado
DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `id` bigint(20) DEFAULT NULL,
  `nombre` varchar(254) DEFAULT NULL,
  `abr` varchar(254) DEFAULT NULL,
  `activo` bigint(20) DEFAULT NULL,
  `latitud` varchar(254) DEFAULT NULL,
  `longitud` varchar(254) DEFAULT NULL,
  `idpais` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.ingredient
DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE IF NOT EXISTS `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price_in` float NOT NULL,
  `price_out` float DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `ingredient_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.item
DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.municipio
DROP TABLE IF EXISTS `municipio`;
CREATE TABLE IF NOT EXISTS `municipio` (
  `id` bigint(20) DEFAULT NULL,
  `nombre` varchar(254) DEFAULT NULL,
  `clavemunici` varchar(254) DEFAULT NULL,
  `idestado` bigint(20) DEFAULT NULL,
  `abr` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.operation
DROP TABLE IF EXISTS `operation`;
CREATE TABLE IF NOT EXISTS `operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `q` float NOT NULL,
  `operation_type_id` int(11) NOT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `is_oficial` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `operation_type_id` (`operation_type_id`) USING BTREE,
  KEY `sell_id` (`sell_id`) USING BTREE,
  CONSTRAINT `operation_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `operation_ibfk_2` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  CONSTRAINT `operation_ibfk_3` FOREIGN KEY (`sell_id`) REFERENCES `sell` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12123 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.operation2
DROP TABLE IF EXISTS `operation2`;
CREATE TABLE IF NOT EXISTS `operation2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingredient_id` int(11) NOT NULL,
  `q` float NOT NULL,
  `operation_type_id` int(11) NOT NULL,
  `sell_id` int(11) DEFAULT NULL,
  `is_oficial` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ingredient_id` (`ingredient_id`) USING BTREE,
  KEY `operation_type_id` (`operation_type_id`) USING BTREE,
  KEY `sell_id` (`sell_id`) USING BTREE,
  CONSTRAINT `operation2_ibfk_1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`),
  CONSTRAINT `operation2_ibfk_2` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  CONSTRAINT `operation2_ibfk_3` FOREIGN KEY (`sell_id`) REFERENCES `sell2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.operation_type
DROP TABLE IF EXISTS `operation_type`;
CREATE TABLE IF NOT EXISTS `operation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.pago
DROP TABLE IF EXISTS `pago`;
CREATE TABLE IF NOT EXISTS `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venta_id` int(11) DEFAULT NULL,
  `venta_codigo` varchar(255) DEFAULT NULL,
  `cajero_id` int(11) DEFAULT NULL,
  `fechaalta` datetime NOT NULL,
  `cantidad` double(14,2) DEFAULT NULL,
  `total` double(14,0) DEFAULT NULL,
  `datooperacion` varchar(25) DEFAULT NULL,
  `tipopago_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `resta` double(14,0) DEFAULT NULL,
  `cambio` double(14,0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3605 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.pay
DROP TABLE IF EXISTS `pay`;
CREATE TABLE IF NOT EXISTS `pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sell_id` int(11) DEFAULT NULL,
  `cajero_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `cantidad` double(14,2) DEFAULT NULL,
  `resta` double(14,2) DEFAULT NULL,
  `paytype_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comentarios` varchar(255) DEFAULT NULL,
  `suc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `cajero_id` (`cajero_id`) USING BTREE,
  CONSTRAINT `pay_ibfk_2` FOREIGN KEY (`cajero_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3605 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.paytype
DROP TABLE IF EXISTS `paytype`;
CREATE TABLE IF NOT EXISTS `paytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.personafisica
DROP TABLE IF EXISTS `personafisica`;
CREATE TABLE IF NOT EXISTS `personafisica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `apellidopaterno` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `apellidomaterno` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celular` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `correoelectronico` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `fechanacimiento` date NOT NULL,
  `identificacionoficial` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `rfc` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contactofacturacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaalta` timestamp NULL DEFAULT NULL,
  `compras` int(11) NOT NULL,
  `ultimacompra` datetime NOT NULL,
  `estatus_id` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.personamoral
DROP TABLE IF EXISTS `personamoral`;
CREATE TABLE IF NOT EXISTS `personamoral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razonsocial` text COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celular` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` text COLLATE utf8_spanish_ci NOT NULL,
  `correoelectronico` text COLLATE utf8_spanish_ci NOT NULL,
  `fechafundacion` date NOT NULL,
  `cedulafiscal` int(11) NOT NULL,
  `rfc` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contactofacturacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaalta` timestamp NULL DEFAULT NULL,
  `fechaedicion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `compras` int(11) NOT NULL,
  `ultimacompra` datetime NOT NULL,
  `estatuscliente_id` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `preparation` varchar(50) NOT NULL,
  `price_in` float NOT NULL,
  `price_out` float DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `presentation` varchar(255) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `use_ingredient` tinyint(1) NOT NULL DEFAULT 1,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `unity_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  KEY `unity_id` (`unity_id`) USING BTREE,
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `product_ibfk_3` FOREIGN KEY (`unity_id`) REFERENCES `unity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.productaago19
DROP TABLE IF EXISTS `productaago19`;
CREATE TABLE IF NOT EXISTS `productaago19` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `preparation` varchar(50) NOT NULL,
  `price_in` float NOT NULL,
  `price_out` float DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `presentation` varchar(255) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `use_ingredient` tinyint(1) NOT NULL DEFAULT 1,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `unity_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.producto
DROP TABLE IF EXISTS `producto`;
CREATE TABLE IF NOT EXISTS `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `preciocompra` float(14,0) NOT NULL,
  `precioventa` float(14,0) NOT NULL,
  `ventas` int(11) NOT NULL,
  `fechaalta` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fechaedicion` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.product_copy1
DROP TABLE IF EXISTS `product_copy1`;
CREATE TABLE IF NOT EXISTS `product_copy1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `preparation` varchar(50) NOT NULL,
  `price_in` float NOT NULL,
  `price_out` float DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `presentation` varchar(255) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `use_ingredient` tinyint(1) NOT NULL DEFAULT 1,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `unity_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  KEY `unity_id` (`unity_id`) USING BTREE,
  CONSTRAINT `product_copy1_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `product_copy1_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `product_copy1_ibfk_3` FOREIGN KEY (`unity_id`) REFERENCES `unity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.product_ingredient
DROP TABLE IF EXISTS `product_ingredient`;
CREATE TABLE IF NOT EXISTS `product_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `q` float DEFAULT NULL,
  `is_required` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `ingredient_id` (`ingredient_id`) USING BTREE,
  CONSTRAINT `product_ingredient_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `product_ingredient_ibfk_2` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.sell
DROP TABLE IF EXISTS `sell`;
CREATE TABLE IF NOT EXISTS `sell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `q` int(11) DEFAULT NULL,
  `is_applied` tinyint(1) NOT NULL DEFAULT 0,
  `mesero_id` int(11) DEFAULT NULL,
  `cajero_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `total` double(14,2) DEFAULT NULL,
  `paytype_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `corte_id` int(11) NOT NULL,
  `suc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `mesero_id` (`mesero_id`) USING BTREE,
  KEY `cajero_id` (`cajero_id`) USING BTREE,
  CONSTRAINT `sell_ibfk_1` FOREIGN KEY (`mesero_id`) REFERENCES `user` (`id`),
  CONSTRAINT `sell_ibfk_2` FOREIGN KEY (`cajero_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6231 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.sell2
DROP TABLE IF EXISTS `sell2`;
CREATE TABLE IF NOT EXISTS `sell2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `operation_type_id` int(11) DEFAULT 2,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `operation_type_id` (`operation_type_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `sell2_ibfk_1` FOREIGN KEY (`operation_type_id`) REFERENCES `operation_type` (`id`),
  CONSTRAINT `sell2_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6292 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.servicio
DROP TABLE IF EXISTS `servicio`;
CREATE TABLE IF NOT EXISTS `servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `preciocompra` float(14,0) NOT NULL,
  `precioventa` float(14,0) NOT NULL,
  `ventas` int(11) NOT NULL,
  `fechaalta` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `fechaedicion` datetime DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.spent
DROP TABLE IF EXISTS `spent`;
CREATE TABLE IF NOT EXISTS `spent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `q` int(11) NOT NULL,
  `concept` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE,
  CONSTRAINT `spent_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.sucursal
DROP TABLE IF EXISTS `sucursal`;
CREATE TABLE IF NOT EXISTS `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `calle` varchar(50) NOT NULL,
  `numint` varchar(10) DEFAULT NULL,
  `numext` int(10) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `cp` int(10) DEFAULT NULL,
  `estado_id` int(10) DEFAULT NULL,
  `municipio_id` int(10) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `horaapertura` time(5) NOT NULL,
  `horacierre` time(5) NOT NULL,
  `fechaalta` datetime NOT NULL,
  `estatus_id` int(5) NOT NULL DEFAULT 1,
  `usuario_id` int(5) DEFAULT NULL,
  `act` int(11) DEFAULT NULL,
  `fechaedicion` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.sucursalbitacora
DROP TABLE IF EXISTS `sucursalbitacora`;
CREATE TABLE IF NOT EXISTS `sucursalbitacora` (
  `id` bigint(20) DEFAULT NULL,
  `name` varchar(254) DEFAULT NULL,
  `calle` varchar(254) DEFAULT NULL,
  `numint` varchar(254) DEFAULT NULL,
  `numext` bigint(20) DEFAULT NULL,
  `colonia` varchar(254) DEFAULT NULL,
  `cp` bigint(20) DEFAULT NULL,
  `estado_id` bigint(20) DEFAULT NULL,
  `municipio_i` bigint(20) DEFAULT NULL,
  `telefono` varchar(254) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `horaapertur` varchar(11) DEFAULT NULL,
  `horacierre` varchar(11) DEFAULT NULL,
  `fechaapertu` date DEFAULT NULL,
  `fechacierre` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `is_active` bigint(20) DEFAULT NULL,
  `diainicio` bigint(20) DEFAULT NULL,
  `diafinal` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.sucursaldiasoperacion
DROP TABLE IF EXISTS `sucursaldiasoperacion`;
CREATE TABLE IF NOT EXISTS `sucursaldiasoperacion` (
  `id` bigint(20) DEFAULT NULL,
  `idsucursal` bigint(20) DEFAULT NULL,
  `iddia` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `is_active` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.sucursalevento
DROP TABLE IF EXISTS `sucursalevento`;
CREATE TABLE IF NOT EXISTS `sucursalevento` (
  `id` bigint(20) DEFAULT NULL,
  `name` varchar(254) DEFAULT NULL,
  `calle` varchar(254) DEFAULT NULL,
  `numint` varchar(254) DEFAULT NULL,
  `numext` bigint(20) DEFAULT NULL,
  `colonia` varchar(254) DEFAULT NULL,
  `cp` bigint(20) DEFAULT NULL,
  `estado_id` bigint(20) DEFAULT NULL,
  `municipio_i` bigint(20) DEFAULT NULL,
  `telefono` varchar(254) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `horaapertur` varchar(11) DEFAULT NULL,
  `horacierre` varchar(11) DEFAULT NULL,
  `fechaapertu` date DEFAULT NULL,
  `fechacierre` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `is_active` bigint(20) DEFAULT NULL,
  `diainicio` bigint(20) DEFAULT NULL,
  `diafinal` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.tipopago
DROP TABLE IF EXISTS `tipopago`;
CREATE TABLE IF NOT EXISTS `tipopago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.tipousuario
DROP TABLE IF EXISTS `tipousuario`;
CREATE TABLE IF NOT EXISTS `tipousuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.unidadproducto
DROP TABLE IF EXISTS `unidadproducto`;
CREATE TABLE IF NOT EXISTS `unidadproducto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.unity
DROP TABLE IF EXISTS `unity`;
CREATE TABLE IF NOT EXISTS `unity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `is_mesero` tinyint(1) NOT NULL DEFAULT 0,
  `is_cajero` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `usertype_id` tinyint(1) NOT NULL DEFAULT 0,
  `suc_id` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.usertype
DROP TABLE IF EXISTS `usertype`;
CREATE TABLE IF NOT EXISTS `usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.usuarios
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `password` text COLLATE utf8_spanish_ci NOT NULL,
  `perfil` text COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `ultimo_login` datetime NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tipousuario_id` int(11) DEFAULT NULL,
  `ultimo_movimiento` datetime NOT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `fechaedicion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla conecta2company.ventas
DROP TABLE IF EXISTS `ventas`;
CREATE TABLE IF NOT EXISTS `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) DEFAULT NULL,
  `fechaalta` datetime NOT NULL,
  `productos` varchar(5000) DEFAULT NULL,
  `total` double(14,2) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `estatusventa` tinyint(1) NOT NULL DEFAULT 0,
  `usuario_id` int(11) DEFAULT NULL,
  `tipopago_id` int(11) DEFAULT NULL,
  `corte_id` int(11) NOT NULL,
  `impuesto` double(14,0) NOT NULL,
  `act` int(11) NOT NULL,
  `comentario` mediumtext DEFAULT NULL,
  `fechaedicion` datetime DEFAULT NULL,
  `neto` double(14,0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6237 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
