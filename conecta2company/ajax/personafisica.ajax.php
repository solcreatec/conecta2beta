<?php

require_once "../controladores/personafisica.controlador.php";
require_once "../modelos/personafisica.modelo.php";

class AjaxPersonaFisica{

	/*=============================================
	EDITAR PERSONA MORAL
	=============================================*/	

	public $idPersonaFisica;

	public function ajaxEditarPersonaFisica(){

		$item = "id";
		$valor = $this->idPersonaFisica;

		$respuesta = ControladorPersonaFisica::ctrMostrarPersonaFisica($item, $valor);

		echo json_encode($respuesta);


	}

}

/*=============================================
EDITAR PERSONA MORAL
=============================================*/	

if(isset($_POST["idPersonaFisica"])){

	$personafisica = new AjaxPersonaFisica();
	$personafisica -> idPersonaFisica = $_POST["idPersonaFisica"];
	$personafisica -> ajaxEditarPersonaFisica();

}