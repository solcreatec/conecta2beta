<?php

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

require_once "../controladores/categorias.controlador.php";
require_once "../modelos/categorias.modelo.php";

require_once "../controladores/sucursales.controlador.php";
require_once "../modelos/sucursales.modelo.php";

class TablaProductos{

 	/*=============================================
 	 MOSTRAR LA TABLA DE PRODUCTOS
  	=============================================*/ 

	public function mostrarTablaProductos(){

		if(isset($_GET["tipoUsuarioOculto"]) && $_GET["tipoUsuarioOculto"] != 1){ 
 
		  		$productos = ControladorProductos::ctrCarritoProductos("sucursal_id", $_GET["sucursalOculto"]);	 
		 }else{ 
		 		$productos = ControladorProductos::ctrMostrarProductos(null, null);	 
		 } 
 
  		// $productos = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);	

  		if(count($productos) == 0){

  			echo '{"data": []}';

		  	return;
  		}
		
  		$datosJson = '{
		  "data": [';

		  for($i = 0; $i < count($productos); $i++){

		  	/*=============================================
 	 		TRAEMOS LA IMAGEN
  			=============================================*/ 

		  	$imagen = "<img src='".$productos[$i]["imagen"]."' width='40px'>";

		  	/*=============================================
 	 		TRAEMOS LA CATEGORÍA
  			=============================================*/ 

		  	$item = "id";
		  	$valor = $productos[$i]["categoria_id"];

		  	$categoria = ControladorCategorias::ctrMostrarCategorias($item, $valor);

		  	/*=============================================
 	 		TRAEMOS LA SUCURSAL
  			=============================================*/ 

		  	$item = "id";
		  	$valor = $productos[$i]["sucursal_id"];

		  	$sucursal = ControladorSucursal::ctrMostrarSucursales($item,$valor, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		  	/*=============================================
 	 		TRAEMOS STOCK Y PERSONALIZAMOS
  			=============================================*/ 

  			if($productos[$i]["stock"] <5){

  				$stock = "<button class='btn btn-danger'>".$productos[$i]["stock"]."</button>";

  			}else if($productos[$i]["stock"] == 5){

  				$stock = "<button class='btn btn-warning'>".$productos[$i]["stock"]."</button>";

  			}else{

  				$stock = "<button class='btn btn-success'>".$productos[$i]["stock"]."</button>";

  			}

		  	/*=============================================
 	 		TRAEMOS PRECIO DE COMPRA Y VENTA Y APLICAMOS FORMATO DE MONEDA PARA QUE APAREZCAN CON EL SIMBOLO $
  			=============================================*/ 

  			$preciocompra = "<b>$ ".number_format($productos[$i]["preciocompra"],2)."</b>";

  			$precioventa = "<b>$ ".number_format($productos[$i]["precioventa"],2)."</b>";
		  	/*=============================================
 	 		TRAEMOS LAS ACCIONES PARA CADA PRODUCTO
  			=============================================*/ 

  			if(isset($_GET["tipoUsuarioOculto"]) && $_GET["tipoUsuarioOculto"] == 3){ 

  				$botones =  "<div class='btn-group'><button class='btn btn-warning btnEditarProducto' idProducto='".$productos[$i]["id"]."' data-toggle='modal' data-target='#modalEditarProducto'><i class='fa fa-pencil'></i></button></div>"; 

  			}else{

  				 $botones =  "<div class='btn-group'><button class='btn btn-warning btnEditarProducto' idProducto='".$productos[$i]["id"]."' data-toggle='modal' data-target='#modalEditarProducto'><i class='fa fa-pencil'></i></button><button class='btn btn-danger btnEliminarProducto' idProducto='".$productos[$i]["id"]."' codigo='".$productos[$i]["codigo"]."' imagen='".$productos[$i]["imagen"]."'><i class='fa fa-times'></i></button></div>"; 

  			}

		 
		  	$datosJson .='[
			      "'.($i+1).'",
			      "'.$imagen.'",
			      "'.$productos[$i]["codigo"].'",
			      "'.$productos[$i]["descripcion"].'",
			      "'.$categoria["nombre"].'",
			      "'.$stock.'",
			      "'.$productos[$i]["ventas"].'",
			      "'.$preciocompra.'",
			      "'.$precioventa.'",
			      "'.$productos[$i]["fechaalta"].'",
			      "'.$sucursal["nombre"].'",
			      "'.$botones.'"
			    ],';


		  }

		  $datosJson = substr($datosJson, 0, -1);

		 $datosJson .=   '] 

		 }';
		
		echo $datosJson;


	}



}

/*=============================================
ACTIVAR TABLA DE PRODUCTOS
=============================================*/ 
$activarProductos = new TablaProductos();
$activarProductos -> mostrarTablaProductos();

