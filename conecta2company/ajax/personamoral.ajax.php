<?php

require_once "../controladores/personamoral.controlador.php";
require_once "../modelos/personamoral.modelo.php";

class AjaxPersonaMoral{

	/*=============================================
	EDITAR PERSONA MORAL
	=============================================*/	

	public $idPersonaMoral;

	public function ajaxEditarPersonaMoral(){

		$item = "id";
		$valor = $this->idPersonaMoral;

		$respuesta = ControladorPersonaMoral::ctrMostrarPersonaMoral($item, $valor);

		echo json_encode($respuesta);


	}

}

/*=============================================
EDITAR PERSONA MORAL
=============================================*/	

if(isset($_POST["idPersonaMoral"])){

	$personamoral = new AjaxPersonaMoral();
	$personamoral -> idPersonaMoral = $_POST["idPersonaMoral"];
	$personamoral -> ajaxEditarPersonaMoral();

}