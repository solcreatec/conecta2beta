<?php

require_once "conexion.php";

class ModeloProductos{

	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/

	static public function mdlMostrarProductos($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}



static public function mdlCategoriaProductos($tabla, $item, $valor, $item2, $valor2){ 
 
// echo "SELECT * FROM $tabla WHERE $item = $valor AND $item2 = $valor2 ORDER BY id DESC"; exit(); 
 
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE act = 1 AND $item = :$item AND $item2 = :$item2 ORDER BY id DESC"); 
 
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR); 
 			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR); 

			$stmt -> execute(); 
 
			return $stmt -> fetch(); 
  
		$stmt -> close(); 
 
		$stmt = null; 
 
	} 


	static public function mdlCarritoProductos($tabla, $item, $valor){ 
 
// echo "SELECT * FROM $tabla WHERE $item = $valor ORDER BY fechaalta DESC"; exit(); 
 
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE act = 1 AND $item = :$item ORDER BY fechaalta DESC"); 
 
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR); 
 
			$stmt -> execute(); 
 
			return $stmt -> fetchAll(); 
 
 
 
		$stmt -> close(); 
 
		$stmt = null; 
 
	} 
	/*=============================================
	REGISTRO DE PRODUCTO
	=============================================*/
	static public function mdlIngresarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(categoria_id, codigo, descripcion, imagen, stock, preciocompra, precioventa, sucursal_id, usuario_id,act) VALUES (:categoria_id, :codigo, :descripcion, :imagen, :stock, :preciocompra, :precioventa, :sucursal_id, :usuario_id,1)");

		$stmt->bindParam(":categoria_id", $datos["categoria_id"], PDO::PARAM_INT);
		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
		$stmt->bindParam(":preciocompra", $datos["preciocompra"], PDO::PARAM_STR);
		$stmt->bindParam(":precioventa", $datos["precioventa"], PDO::PARAM_STR);
		$stmt->bindParam(":sucursal_id", $datos["sucursal_id"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	EDITAR PRODUCTO
	=============================================*/
	static public function mdlEditarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET categoria_id = :categoria_id, descripcion = :descripcion, imagen = :imagen, stock = :stock, preciocompra = :preciocompra, precioventa = :precioventa WHERE codigo = :codigo");

		$stmt->bindParam(":categoria_id", $datos["categoria_id"], PDO::PARAM_INT);
		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
		$stmt->bindParam(":descripcion", $datos["descripcion"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen", $datos["imagen"], PDO::PARAM_STR);
		$stmt->bindParam(":stock", $datos["stock"], PDO::PARAM_STR);
		$stmt->bindParam(":preciocompra", $datos["preciocompra"], PDO::PARAM_STR);
		$stmt->bindParam(":precioventa", $datos["precioventa"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	BORRAR PRODUCTO
	=============================================*/

	static public function mdlEliminarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR PRODUCTO
	=============================================*/

	static public function mdlActualizarProducto($tabla, $item1, $valor1, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR SUMA VENTAS
	=============================================*/	

	static public function mdlMostrarSumaVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT SUM(ventas) as total FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}


}