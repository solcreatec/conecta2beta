<?php

require_once "conexion.php";

class ModeloVentas{

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/

	static public function mdlMostrarVentas($tabla, $item, $valor){

		if($item != null){
			$stmt = Conexion::conectar()->prepare("
				SELECT * FROM $tabla 
				WHERE $item = :$item 
				ORDER BY fechaalta ASC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		
		$stmt -> close();

		$stmt = null;

	}

	static public function mdlCodigoVentas($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("
				SELECT * FROM $tabla 
				WHERE $item = :$item 
				ORDER BY id ASC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	FUNCION DINAMICA DE PERSONALIZACION DE CONSULTAS
	=============================================*/	

	static public function mdlVentasMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		if($item != null){
			if ($item2 != null) {
				if ($item3 !=null) {
					if ($item4 !=null) {
						if ($item5 !=null) {
						// echo "5".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4 
												AND $item5 = :$item5 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item5, $valor5, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();

					}else{
					// echo "4".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
												AND $item4 = :$item4
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}

					}else{
					// echo "3".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2 
												AND $item3 = :$item3 
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
						}
				}else{

					// echo "2".$prueba; exit();
					$stmt = Conexion::conectar()->prepare("
						SELECT * FROM $tabla WHERE $item = :$item 
												AND $item2 = :$item2
						ORDER BY fechaalta DESC");

					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

					$stmt -> execute();

					return $stmt -> fetchAll();
				}
			}else{

				// echo "1".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla 
															WHERE $item = :$item 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();
		}
		}else{
			// echo "0".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	REGISTRO DE VENTA
	=============================================*/

	static public function mdlIngresarVenta($tabla, $datos){

// 		$stmt = "INSERT INTO $tabla(codigo, usuario_id, productos, impuesto, neto, total, fechaalta, comentario, sucursal_id, estatusventa, act)  VALUES (".$datos['codigo'].", ".$datos['usuario_id'].", '".$datos['productos']."', ".$datos['impuesto'].", ".$datos['neto'].", ".$datos['total'].", '".$datos['fechaalta']."', '".$datos['comentario']."', ".$datos['sucursal_id']. ", 0, 1)";
// echo $stmt; exit();
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(codigo, usuario_id, productos, impuesto, neto, total, fechaalta, comentario, sucursal_id, estatusventa, act, tipoventa_id)  VALUES (:codigo, :usuario_id, :productos, :impuesto, :neto, :total, :fechaalta, :comentario, :sucursal_id, 0, 1, 1)");

		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_INT);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_INT);
		$stmt->bindParam(":productos", $datos["productos"], PDO::PARAM_STR);
		$stmt->bindParam(":impuesto", $datos["impuesto"], PDO::PARAM_STR);
		$stmt->bindParam(":neto", $datos["neto"], PDO::PARAM_STR);
		$stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
		$stmt->bindParam(":fechaalta", $datos["fechaalta"], PDO::PARAM_STR);
		$stmt->bindParam(":comentario", $datos["comentario"], PDO::PARAM_STR);
		$stmt->bindParam(":sucursal_id", $datos["sucursal_id"], PDO::PARAM_INT);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR VENTA CON CORTE CORRESPONDIENTE
	=============================================*/

	static public function mdlActualizarVenta($tabla, $item, $valor, $item1, $valor1, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		if($item1 != null){
			if ($item2 != null) {
				if ($item3 !=null) {
					if ($item4 !=null) {
						if ($item5 !=null) {
						// echo "5".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :$item 
											WHERE $item1 = :item1 
												AND $item2 = :item2 
												AND $item3 = :item3 
												AND $item4 = :item4 
												AND $item5 = :item5");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":item3", $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":item4", $valor4, PDO::PARAM_STR);
					$stmt -> bindParam(":item5", $valor5, PDO::PARAM_STR);

					}else{
					// echo "4".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :item 
											WHERE $item1 = :item1 
												AND $item2 = :item2 
												AND $item3 = :item3 ");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":item3", $valor3, PDO::PARAM_STR);
					$stmt -> bindParam(":item4", $valor4, PDO::PARAM_STR);

						}

					}else{
					// echo "3".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :item 
											WHERE $item1 = :item1 
												AND $item2 = :item2 
												AND $item3 = :item3 ");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);
					$stmt -> bindParam(":item3", $valor3, PDO::PARAM_STR);

						}
				}else{

					// echo "2".$prueba; exit();
										$stmt = Conexion::conectar()->prepare("
											UPDATE $tabla SET  $item = :item 
											WHERE $item1 = :item1 
												AND $item2 = :item2");

					$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);
					$stmt -> bindParam(":item2", $valor2, PDO::PARAM_STR);

				}
			}else{

				// echo "1".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item = :item  
															WHERE $item1 = :item1 
				ORDER BY fechaalta DESC");

			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":item1", $valor1, PDO::PARAM_STR);

		}
		}else{
			// echo "0".$prueba; exit();
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item = :item");
			$stmt -> bindParam(":item", $valor, PDO::PARAM_STR);
		}

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;
	}

	/*=============================================
	EDITAR VENTA
	=============================================*/

	static public function mdlEditarVenta($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET usuario_id = :usuario_id, productos = :productos, comentario = :comentario, fechaedicion = :fechaedicion, total= :total WHERE codigo = :codigo AND id = :id");

		$stmt->bindParam(":codigo", $datos["codigo"], PDO::PARAM_STR);
		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_STR);
		$stmt->bindParam(":usuario_id", $datos["usuario_id"], PDO::PARAM_STR);
		$stmt->bindParam(":productos", $datos["productos"], PDO::PARAM_STR);
		$stmt->bindParam(":comentario", $datos["comentario"], PDO::PARAM_STR);
		$stmt->bindParam(":total", $datos["total"], PDO::PARAM_STR);
		$stmt->bindParam(":fechaedicion", $datos["fechaedicion"], PDO::PARAM_STR);


		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ELIMINAR VENTA
	=============================================*/

	static public function mdlEliminarVenta($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	RANGO FECHAS
	=============================================*/	

	static public function mdlRangoFechasVentas($tabla, $fechaInicial, $fechaFinal){

		$sql = "SELECT * FROM $tabla";
		$order = " ORDER BY fechaalta DESC";
		$adicional = " AND sucursal_id = ".$_SESSION["sucursal_id"]." AND usuario_id = ".$_SESSION['id']." AND act = 1";

		if($fechaInicial == null){

			if ($_SESSION['tipousuario_id'] != 1) {
				$sql .= $adicional.$order;
			}else{
				$sql .= $order;
			}

			$stmt = Conexion::conectar()->prepare($sql);

			// $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY fechaalta ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();	


		}else if($fechaInicial == $fechaFinal){

			// $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fechaalta like '%$fechaFinal%' ORDER BY fechaalta DESC");

			$sql .= " WHERE fechaalta like '%$fechaFinal%'";

			if ($_SESSION['tipousuario_id'] != 1) {
				$sql .= $adicional.$order;
			}else{
				$sql .= $order;
			}

			$stmt = Conexion::conectar()->prepare($sql);

			$stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$fechaActual = new DateTime();
			$fechaActual ->add(new DateInterval("P1D"));
			$fechaActualMasUno = $fechaActual->format("Y-m-d");

			$fechaFinal2 = new DateTime($fechaFinal);
			$fechaFinal2 ->add(new DateInterval("P1D"));
			$fechaFinalMasUno = $fechaFinal2->format("Y-m-d");

			if($fechaFinalMasUno == $fechaActualMasUno){

				$sql .= " WHERE fechaalta BETWEEN '$fechaInicial' AND '$fechaFinalMasUno'";

			if ($_SESSION['tipousuario_id'] != 1) {
				$sql .= $adicional.$order;
			}else{
				$sql .= $order;
			}

				$stmt = Conexion::conectar()->prepare($sql);

				// $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fechaalta BETWEEN '$fechaInicial' AND '$fechaFinalMasUno' ORDER BY fechaalta DESC");

			}else{


				$sql .= " WHERE fechaalta BETWEEN '$fechaInicial' AND '$fechaFinal'";

			if ($_SESSION['tipousuario_id'] != 1) {
				$sql .= $adicional.$order;
			}else{
				$sql .= $order;
			}
			
				$stmt = Conexion::conectar()->prepare($sql);
				
				// $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fechaalta BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY fechaalta DESC");

			}
		
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

	}



	/*=============================================
	REPORTEO
	=============================================*/	

	static public function mdlReporteo($tabla, $fechaInicial, $fechaFinal, $formapago,$estatus,$usuario,$sucursal){

// echo "modelo".$fechaInicial.$fechaFinal."suc".$sucursal."us".$usuario."est".$estatus."fp".$formapago; exit();

		$sql = "SELECT * FROM $tabla";
		$order = " ORDER BY ventas.fechaalta DESC";

		if ($formapago!= null) {

			$sql .= " LEFT JOIN pago ON pago.venta_id = ventas.id ";
			$adicional = "WHERE tipoventa_id = 1 AND pago.tipopago_id = $formapago ";		
		}else{
			$adicional = " WHERE tipoventa_id = 1 ";
		}


		if ($estatus!= null) {
			$adicional .= "AND ventas.estatusventa = $estatus ";
		}

		if ($sucursal!= null) {
			$adicional .= "AND ventas.sucursal_id = $sucursal ";
		}

		if ($usuario!= null) {
			$adicional .= "AND ventas.usuario_id = $usuario ";
		}

		if($fechaInicial == null){

			$sql .= $adicional.$order;
			// echo "NULL ".$sql; exit();
		}else if($fechaInicial == $fechaFinal){

			$adicional .= " AND fechaalta like '%$fechaFinal%'";

			$sql .= $adicional.$order;
			// echo "2 ".$sql; exit();
		}else{

			$fechaActual = new DateTime();
			$fechaActual ->add(new DateInterval("P1D"));
			$fechaActualMasUno = $fechaActual->format("Y-m-d");

			$fechaFinal2 = new DateTime($fechaFinal);
			$fechaFinal2 ->add(new DateInterval("P1D"));
			$fechaFinalMasUno = $fechaFinal2->format("Y-m-d");

			if($fechaFinalMasUno == $fechaActualMasUno){

				$adicional .= " AND fechaalta BETWEEN '$fechaInicial' AND '$fechaFinalMasUno'";

				$sql .= $adicional.$order;

			}else{


				$adicional .= " AND fechaalta BETWEEN '$fechaInicial' AND '$fechaFinal'";

				$sql .= $adicional.$order;
			}
		
// echo "3".$sql; exit();

		}

			$stmt = Conexion::conectar()->prepare($sql);

			$stmt -> execute();

			return $stmt -> fetchAll();	
	}


	/*=============================================
	SUMAR EL TOTAL DE VENTAS
	=============================================*/

	static public function mdlSumaTotalVentas($tabla){	

		// $stmt = Conexion::conectar()->prepare("SELECT SUM(neto) as total FROM $tabla");
		$stmt = Conexion::conectar()->prepare("SELECT SUM(total) as total FROM $tabla
			WHERE estatusventa = 1");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

	static public function mdlSumaVentas($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4){


	$sql = "SELECT SUM(total) as total, count(*) as contador FROM $tabla";

	if ($item != null) {

		$sql .= " WHERE $item = :$item ";

		if ($item2 != null) {

			$sql .= "AND $item2 = :$item2 ";

			if ($item3 != null) {

				$sql .= "AND $item3 = :$item3 ";

				if ($item4 != null) {

				$sql .= "AND $item4 = :$item4 ";
				// echo $sql; exit();
				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

				} else{

				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
				}


			} else{

				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			}

		} else{

			$stmt = Conexion::conectar()->prepare($sql);
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		}

	}else{

		$stmt = Conexion::conectar()->prepare($sql);
	}	

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}


	static public function mdlSumaVentasHoy($tabla, $fecha, $item, $valor, $item2, $item3, $valor2){
		echo "SELECT SUM(total) as total FROM $tabla WHERE fechaalta >= '$fecha 00:00' AND fechaalta <= '$fecha 23:59'"; exit();

		$sql = "SELECT SUM(total) as total, count(*) as contador FROM $tabla WHERE fechaalta >= '$fecha 00:00' AND fechaalta <= '$fecha 23:59'";

if ($item != null) {

		$sql .= " AND $item = :$item ";

		if ($item2 != null) {

			$sql .= "AND $item2 = :$item2 ";

			if ($item3 != null) {

				$sql .= "AND $item3 = :$item3 ";

				if ($item4 != null) {

				$sql .= "AND $item4 = :$item4 ";
				// echo $sql; exit();
				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

				} else{

				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
				}


			} else{

				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			}

		} else{

			$stmt = Conexion::conectar()->prepare($sql);
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		}

	}else{

		$stmt = Conexion::conectar()->prepare($sql);
	}	


		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	OBTENER EL ULTIMO ID DEL CORTE GENERADO 
	=============================================*/

	static public function mdlMaxIDVentas($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4){

	$sql = "SELECT max(id) as idmax FROM $tabla";

	if ($item != null) {

		$sql .= " WHERE $item = :$item ";

		if ($item2 != null) {

			$sql .= "AND $item2 = :$item2 ";

			if ($item3 != null) {

				$sql .= "AND $item3 = :$item3 ";

				if ($item4 != null) {

				$sql .= "AND $item4 = :$item4 ";
				// echo $sql; exit();
				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item4, $valor4, PDO::PARAM_STR);

				} else{

				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item3, $valor3, PDO::PARAM_STR);
				}


			} else{

				$stmt = Conexion::conectar()->prepare($sql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			}

		} else{

			$stmt = Conexion::conectar()->prepare($sql);
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
		}

	}else{

		$stmt = Conexion::conectar()->prepare($sql);
	}	

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

}