<?php

require_once "conexion.php";

class ModeloPersonaFisica{

	/*=============================================
	CREAR CONTACTO
	=============================================*/

	static public function mdlIngresarPersonaFisica($tabla, $datos){

		// echo $datos["apellidomaterno"];
		// "INSERT INTO ".$tabla."(nombre, apellidopaterno, apellidomaterno, rfc, correoelectronico, telefono, celular, fechanacimiento)values(".$datos["fechanacimiento"];
		// exit();
		$fechaalta = date('Y-m-d H:i:s');
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, apellidopaterno, apellidomaterno, rfc, correoelectronico, telefono, celular, fechanacimiento, fechaalta, usuario_id) VALUES (:nombre, :apellidopaterno, :apellidomaterno, :rfc, :correoelectronico, :telefono, :celular, :fechanacimiento, :fechaalta, :usuario_id)");

		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":apellidopaterno", $datos["apellidopaterno"], PDO::PARAM_STR);
		$stmt->bindParam(":apellidomaterno", $datos["apellidomaterno"], PDO::PARAM_STR);
		$stmt->bindParam(":rfc", $datos["rfc"], PDO::PARAM_STR);
		$stmt->bindParam(":correoelectronico", $datos["correoelectronico"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":celular", $datos["celular"], PDO::PARAM_STR);
		$stmt->bindParam(":fechanacimiento", $datos["fechanacimiento"], PDO::PARAM_STR);
		$stmt->bindParam(":fechaalta", $fechaalta, PDO::PARAM_STR);
		$stmt->bindParam(":usuario_id", $_SESSION["id"], PDO::PARAM_INT);
		

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR CONTACTOS
	=============================================*/

	static public function mdlMostrarPersonaFisica($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	EDITAR CONTACTO
	=============================================*/

	static public function mdlEditarPersonaFisica($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, :apellidopaterno, apellidomaterno, rfc = :rfc, correoelectronico = :correoelectronico, telefono = :telefono, celular = :celular, fechanacimiento = :fechanacimiento WHERE id = :id");

		$stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":rfc", $datos["rfc"], PDO::PARAM_INT);
		$stmt->bindParam(":correoelectronico", $datos["correoelectronico"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":celular", $datos["celular"], PDO::PARAM_STR);
		$stmt->bindParam(":fechanacimiento", $datos["fechanacimiento"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ELIMINAR CONTACTO
	=============================================*/

	static public function mdlEliminarPersonaFisica($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR CONTACTO
	=============================================*/

	static public function mdlActualizarPersonaFisica($tabla, $item1, $valor1, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

}