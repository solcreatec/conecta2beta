 <?php 
$sucursal = ControladorSucursal::ctrMostrarSucursales("id",$_SESSION['sucursal_id']);
  ?>

 <header class="main-header">
 
   <link rel="stylesheet" href="vistas/dist/css/menuLogin.css">

	<!--=====================================
	LOGOTIPO
	======================================-->
    <a href="inicio" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>W</b> L
      	<sup>
      		<small>
      			<span class="label label-success">v1.0</span>
      	    </small>
      	</sup>
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Wisper</b> Land
      	<sup>
      		<small>
      			<span class="label label-success">v1.0</span>
      	    </small>
      	</sup>

      </span>
    </a>


	<nav class="navbar navbar-static-top" role="navigation">

		<!-- Botón de navegación -->

	 	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        	
        	<span class="sr-only">Toggle navigation</span>
      	
      	</a>
	
		<!-- perfil de usuario -->

		<div class="navbar-custom-menu">
				
			<ul class="nav navbar-nav">
				
               <li class="dropdown user user-menu">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<strong>
                    <?php

					if($_SESSION["foto"] != ""){

						echo '<img src="'.$_SESSION["foto"].'" class="user-image">';

					}else{


						echo '<img src="vistas/img/usuarios/default/anonymous.png" class="user-image">';

					}

					?>
						
						<span class="hidden-xs"><?php  echo $_SESSION["nombre"]; ?></span>

                        </strong>
                        
                        <span>&nbsp;<i class="glyphicon glyphicon-chevron-down"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="navbar-login">
                                <div class="row">
                                    <div class="col-lg-4">
										<?php

										if($_SESSION["foto"] != ""){

										echo '<img src="'.$_SESSION["foto"].'" width="90px">';

										}else{


										echo '<img src="vistas/img/usuarios/default/anonymous.png" width="90px">';

										}

										?>                                  
                    				</div>
                                    <div class="col-lg-8 pull-right">
                                        <p class="text-left"><strong><?php  echo $_SESSION["nombre"]; ?></strong></p>
                                        <!-- <p class="text-left small"><?php echo $sucursal['nombre']; ?></p> -->
                                        <p class="text-left">
                                            <a href="salir" class="btn btn-primary btn-block btn-sm">Cerrar Sesión</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
<!--                         <li>
                            <div class="navbar-login navbar-login-session">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                        	<a href="#" class="btn btn-primary btn-block">Mi Perfil</a>
                                            <a href="#" class="btn btn-danger btn-block">Cambiar Contraseña</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul> -->
                </li>
            </ul>
             
	</div>

  </nav>

 </header>