<?php

if($_SESSION["tipousuario_id"] == 3){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

// $xml = ControladorPedidos::ctrDescargarXML();

// if($xml){

//   rename($_GET["xml"].".xml", "xml/".$_GET["xml"].".xml");

//   echo '<a class="btn btn-block btn-success abrirXML" archivo="xml/'.$_GET["xml"].'.xml" href="Pedidos">Se ha creado correctamente el archivo XML <span class="fa fa-times pull-right"></span></a>';

// }

?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar Pedidos
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Administrar Pedidos</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
  
        <a href="crear-pedido">

          <button class="btn btn-primary">
            
          <i class="fa fa-plus-circle"></i>
          &nbsp;&nbsp;Agregar

          </button>

        </a>

         <button type="button" class="btn btn-default pull-right" id="daterange-btn">
           
            <span>
              <i class="fa fa-calendar"></i> 

              <?php

                if(isset($_GET["fechaInicial"])){

                  echo $_GET["fechaInicial"]." - ".$_GET["fechaFinal"];
                
                }else{
                 
                  echo 'Rango de fecha';

                }

              ?>
            </span>

            <i class="fa fa-caret-down"></i>

         </button>

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
          <?php if($_SESSION["tipousuario_id"] != 1){ ?>
           <th style="width:10px"></th>
         <?php } ?>
           <th style="width:10px">#</th>
           <th>Código</th>
           <th>Fecha</th>
           <th>Sucursal</th>
           <th>Vendedor</th>
           <th>Total</th>
           <th style="width:10px;">Estatus</th>
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          if(isset($_GET["fechaInicial"])){

            $fechaInicial = $_GET["fechaInicial"];
            $fechaFinal = $_GET["fechaFinal"];

          }else{

            $fechaInicial = date('yy-m-d');
            $fechaFinal = date('yy-m-d');

          }

          // echo $fechaInicial.$fechaFinal;

          $respuesta = ControladorPedidos::ctrRangoFechasPedidos($fechaInicial, $fechaFinal);

          foreach ($respuesta as $key => $value) {


            $respuestaUsuario = ControladorUsuarios::ctrMostrarUsuarios("id", $value["usuario_id"]);

            $sucursal = ControladorSucursal::ctrMostrarSucursales("id",$value["sucursal_id"], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

           echo '<tr>';

           if($_SESSION["tipousuario_id"] != 1){
           echo  '<td>
                      <button class="btn btn-default btnVerPedido" idPedido="'.$value["id"].'"><i class="fa fa-eye"></i></button>
                  </td>';
                }

          echo '<td>'.($key+1).'</td>

                  <td>'.$value["codigo"].'</td>
                  
                  <td>'.strftime("%A, %d de %B de %G, <B>%H:%M</B>", strtotime($value["fechaalta"])).'</td>

                  <td>'.$sucursal["nombre"].'</td>

                  <td>'.$respuestaUsuario["nombre"].'</td>

                  <td><B>$ '.number_format($value["total"],2).'</td>
                  
                  <td>';

                      if($value['estatusPedido'] == 1) { 

                        echo "<p class='label label-success'>
                                <i class='glyphicon glyphicon-ok'></i> 
                                &nbsp;&nbsp;Finalizado
                              </p>"; 
                            }
                      else { 

                        echo "<p class='label label-warning'>
                               <i class='glyphicon glyphicon-time'></i> 
                               &nbsp;&nbsp;Pendiente
                              </p>"; 
                            }
            echo '</td>';


        if($_SESSION["tipousuario_id"] == 1 && $value['corte_id'] == 0){

              echo '<td style="width:140px;">';
        }else{
          echo '<td style="width:15px;">';
        }

                    echo'<div class="btn-group">';

                      // <a class="btn btn-success" href="index.php?ruta=Pedidos&xml='.$value["codigo"].'">xml</a>
                        
                    // echo '<button class="btn btn-info btnImprimirFactura" codigoPedido="'.$value["codigo"].'">

                    //     <i class="fa fa-print"></i>

                    //   </button>';

                      echo '
                      <button class="btn btn-info btnVerPedido" idPedido="'.$value["id"].'"><i class="fa fa-eye"></i></button>';

                  if($_SESSION["tipousuario_id"] == 1 && $value['corte_id'] == 0){


                      echo '<button class="btn btn-warning btnEditarPedido" idPedido="'.$value["id"].'"><i class="fa fa-pencil"></i></button>';

                      echo '<button class="btn btn-danger btnEliminarPedido" idPedido="'.$value["id"].'"><i class="fa fa-times"></i></button>';

                    }

                    echo '</div>';

                 echo ' </td>';
                echo '</tr>';
            }

        ?>
               
        </tbody>

       </table>

       <?php

      $eliminarPedido = new ControladorPedidos();
      $eliminarPedido -> ctrEliminarPedido();

      ?>
       

      </div>

    </div>

  </section>

</div>



