<?php

    $venta = ControladorVentas::ctrMostrarVentas("id", $_GET["idVenta"]);
    $vendedor = ControladorUsuarios::ctrMostrarUsuarios("id", $venta["usuario_id"]);      
    $pagos = ControladorPago::ctrMostrarPagos("venta_id", $_GET["idVenta"]);

    $sumaPagos = ControladorPago::ctrSumaPago("venta_id", $_GET["idVenta"]);
    $pendiente = $venta['total'] - $sumaPagos['total']; 
    // echo $pendiente; exit();

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1> Detalle Venta</h1>

    <ol class="breadcrumb">
      
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Detalle Venta</li>
    
    </ol>

  </section>


  <section class="content"><!-- COMIENZO DEL DETALLE DE LAS VENTAS -->

<!-- ALERTAS DE INFORMACIÓN DE LA VENTA -->

  <?php if ($venta['estatusventa']==1) { ?>
      <p class="alert alert-success">
      <i class="glyphicon glyphicon-ok-sign"></i> 
      <B>&nbsp;&nbsp;Esta venta esta COMPLETADA.</B>
      </p>
  <?php } else{ ?>
      <p class="alert alert-warning">
      <i class="glyphicon glyphicon-time">
      </i><B>&nbsp;&nbsp;Esta venta esta PENDIENTE, agrega un pago para finalizarla.</B>

        <a href="index.php?ruta=editar-venta&idVenta=<?php echo $_GET['idVenta'] ?>" class="pull-right">
          <i class="fa fa-pencil-square-o" style="font-size: 25px" aria-hidden="true" >
          </i>
        </a>

    </p>
  <?php }?>

  <?php if ($venta['corte_id']>0) { ?>
    <p class="alert alert-success">
    <i class="glyphicon glyphicon-ok-sign"></i> 
    <B>&nbsp;&nbsp;Esta venta pertenece al corte #
    <?php echo $venta['corte_id']; ?>, por lo tanto no se puede MODIFICAR O CANCELAR.</B>
    </p>
  <?php } else{ ?>
      <p class="alert alert-warning">
      <i class="glyphicon glyphicon-time">
      </i><B>&nbsp;&nbsp;Esta venta aun NO ESTA EN CORTE.</B></p>
  <?php }?>

<!-- FIN ALERTAS DE INFORMACIÓN DE LA VENTA -->

  <div class="row"><!--INICIO DE LA FILA PRINCIPAL -->

<!--=====================================
EL FORMULARIO
======================================-->
      
    <div class="col-lg-4 col-xs-12">
        
      <div class="box box-success">

        <div class="box-body">
  

      <!--=====================================
      DETALLE VENTA PARA PANTALLA GRANDE
      ======================================-->
    <div >
                  
      <table class="table">
        <thead>
          <tr>
            <th>Codigo Venta</th>
          </tr>
        </thead>

        <tbody>
      <tr>
       
        <td>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-hashtag" aria-hidden="true"></i>
            </span>

            <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $venta["codigo"]; ?>" readonly>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <!--==================================================-->

      <table class="table">
        <thead>
          <tr>
            <th>Fecha Venta</th>
          </tr>
        </thead>

        <tbody>
      <tr>
        <td width="63%">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
            </span>
            <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo strftime("%A, %d de %B de %G, %H:%M", strtotime($venta["fechaalta"])); ?>" readonly>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <!--==================================================-->

  <table class="table">
    <thead>
      <tr>
        <th>Vendedor</th>
      </tr>
    </thead>

    <tbody>
      <tr>
         <td>
<div class="input-group">
  
  <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i>
</span>

 <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $vendedor["nombre"]; ?>" readonly>

</div>
</td>
</tr>
</tbody>
</table>

  <!--==================================================-->

<?php if ($venta['comentario'] != NULL) { ?>
<div class="hidden-sm hidden-xs">
  
    <table class="table">
    <thead>
      <tr>
        <th>Comentario</th>
      </tr>
    </thead>

    <tbody>
      <tr>
         <td>
<div class="input-group">

 <textarea name="comentario" class="form-control" id="comentario" readonly>
    <?php echo $venta["comentario"]; ?>
 </textarea>

</div>
</td>
</tr>
</tbody>
</table>

</div>

<div class="hidden-lg hidden-xl">
  
    <table class="table">
    <thead>
      <tr>
        <th>Comentario</th>
      </tr>
    </thead>

    <tbody>
      <tr>
         <td>
<div class="input-group">

 <textarea name="comentarios" class="form-control" id="comentarios" style="width: 185%" readonly>
    <?php echo $venta["comentario"]; ?>
 </textarea>

</div>
</td>
</tr>
</tbody>
</table>

</div>
  <!--==================================================-->
<?php } ?>

            <!--=====================================
            ENTRADA IMPUESTOS Y TOTAL
            ======================================-->
                  
            <div class="col-xs-8 pull-right">

            <table class="table">

              <thead>

                <tr>
                <th>Total</th>      
                </tr>

              </thead>

              <tbody>

                <tr>

                  <td style="width: 90%">

                  <div class="input-group">

                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" value="<?php echo number_format($venta["total"],2); ?>" readonly required>                            

                  </div>

                  </td>

                </tr>

              </tbody>

            </table>
<?php if ($pagos != null){  ?>
            <table class="table">

              <thead>

                <tr>
                <th>Pagado</th>      
                </tr>

              </thead>

              <tbody>

                <tr>

                  <td style="width: 90%">

                  <div class="input-group">

                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" value="<?php echo number_format($sumaPagos["total"],2); ?>" readonly required>

                  </div>

                  </td>

                </tr>

              </tbody>

            </table>
<?php } 

if ($pendiente >0) {?>
            <table class="table">

              <thead>

                <tr>
                <th>Pendiente</th>      
                </tr>

              </thead>

              <tbody>

                <tr>

                  <td style="width: 90%">

                  <div class="input-group">

                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" value="<?php echo number_format($pendiente,2); ?>" readonly required>

                  </div>

                  </td>

                </tr>

              </tbody>

            </table>
<?php } ?>
            </div>
</div>
  <?php

  // if($venta['total']>=50){

  // echo '<a href="vistas/modulos/validarconcurso.php?idVenta='.$_GET["idVenta"].'">

  // <button class="btn btn-success" style="margin-top:5px">
  //   <i class="fa fa-ticket"></i>
  // Generar Boleto</button>

  // </a>
  // ';

  // }
  // else{

  // echo '<a href="#">';

  // }         

  ?>


</div>
</div>
 </div>

<!--=====================================
FIN EL FORMULARIO
======================================-->



<!--=====================================
LISTA DE PRODUCTOS DE LA VENTA
======================================-->

      <div class="col-lg-8 col-xs-12">
        
        <div class="box box-warning">

          <div class="box-header with-border"><h4><B>
            <i class="fa fa-list" aria-hidden="true"></i>
                &nbsp;Lista de Productos</B></h4>
          </div>

            <div class="box-body">
            
      <!--=====================================
      ENTRADA PARA AGREGAR PRODUCTO
      ======================================--> 

       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th style="width:15px">Código</th>
           <th>Producto</th>
           <th style="width:10px">Cantidad</th>
           <th style="width:85px">Precio</th> 
           <th>Total</th>

         </tr> 

        </thead>
                <?php

                $listaProducto = json_decode($venta["productos"], true);

                foreach ($listaProducto as $key => $value) {

                  $item = "id";
                  $valor = $value["id"];
                  $orden = "id";

                  $respuesta = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);

                  echo '<tr>

                  <td>'.($key+1).'</td>
                  <td>'.$respuesta["codigo"].'</td>';

                  echo '<td>'.$value["descripcion"].'</td>

                  <td>'.$value["cantidad"].'</td>


                  <td>$ '.number_format($respuesta["precioventa"],2).'</td>

                  <td><B>$ '.number_format($value["total"],2).'</td>
                  </tr>
                  ';
         }?>
        </table>

          </div>

        </div>

<?php if ($pagos != null){ ?>
  
        <div class="box box-info">

          <div class="box-header with-border"><h4><B>
            <i class="fa fa-list" aria-hidden="true"></i>
                &nbsp;Lista de Pagos</B></h4></div>

            <div class="box-body">
            
                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

          
       <table class="table table-bordered table-striped dt-responsive" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th> Fecha</th>
           <th> Cajero</th>
           <th> Tipo Pago</th>
           <th> Pago</th> 
           <th> Comentario</th>
           <th> Resta</th>

         </tr> 

        </thead>
                <?php

                foreach ($pagos as $key => $value) {

                  $tipopago = ControladorTipoPago::ctrMostrarTipoPago("id",$value["tipopago_id"]); 
                  $cajero = ControladorUsuarios::ctrMostrarUsuarios("id", $value["usuario_id"]);

                  echo '<tr>
                  <td>'.($key+1).'</td>
                  <td>'.$value["fechaalta"].'</td>
                  <td>'.$cajero["nombre"].'</td>
                  <td>'.$tipopago["nombre"].'</td>
                  <td>$ '.number_format($value["cantidad"],2).'</td>';

                  if ($value['tipopago_id'] == 1) {

                    echo '<td> SU CAMBIO FUE <b>$'.$value["datooperacion"].'</b></td>';

                  }else if ($value['tipopago_id'] == 2 || $value['tipopago_id'] == 3){

                    echo '<td>ID TRANSACCION: <b>'.$value["datooperacion"].'</b></td>';

                  } else{

                    echo '<td><b>'.$value["datooperacion"].'</b></td>';
                  }

                  
                  echo '<td><B>$ '.number_format($value["resta"],2).'</td>
                  </tr>';
         }?>
</table>
</div>
</div>
<?php } ?>
</div>

        </div><!--FIN DE LA FILA PRINCIPAL -->
  </section><!--FIN DEL DETALLE DE LAS VENTAS -->
</div>