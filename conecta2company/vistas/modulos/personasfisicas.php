<?php

if($_SESSION["tipousuario_id"] == 3){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Personas Fisicas
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Personas Fisicas</li>
    
    </ol>

  </section>


  <section class="content">

    <div class="box">

      <div class="box-header with-border">

<!--         <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarPersonaFisica"> -->
          
        <a href="crearpersonafisica">

          <button class="btn btn-primary" >
           <i class="fa fa-plus"></i>
          &nbsp;&nbsp;Agregar cliente
          </button>
        </a>

<!--         </button> -->

      </div>

      <div class="box-body">
        
       <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th>Nombre</th>
           <th>Teléfono</th>
           <!-- <th>Dirección</th> -->
<!--            <th>Documento ID</th> -->
           <th>Correo electronico</th>
           
           
           <th>Fecha Nacimiento</th> 
           <th>Total compras</th>
           <th>Última compra</th>
           <!-- <th>Ingreso al sistema</th> -->
           <th>Acciones</th>

         </tr> 

        </thead>

        <tbody>

        <?php

          $item = null;
          $valor = null;

          $clientes = ControladorPersonaFisica::ctrMostrarPersonaFisica($item, $valor);

          foreach ($clientes as $key => $value) {
            
// <td>'.$value["direccion"].'</td>
                                // <td>'.$value["documento"].'</td>
                                // <td>'.$value["fecha"].'</td>
            echo '<tr>

                    <td>'.($key+1).'</td>

                    <td>'.$value["nombre"].'</td>

                    <td>'.$value["telefono"].'</td>

                    <td>'.$value["correoelectronico"].'</td>

                    <td>'.$value["fechanacimiento"].'</td>             

                    <td>'.$value["compras"].'</td>

                    <td>'.$value["ultimacompra"].'</td>

                    <td>

                      <div class="btn-group">
                            <button class="btn btn-info btnVerPersonaFisica" idPersonaFisica="'.$value["id"].'"><i class="fa fa-eye"></i></button>
                         <button class="btn btn-warning btnEditarPersonaFisica" data-toggle="modal" data-target="#modalEditarPersonaFisica" idPersonaFisica="'.$value["id"].'"><i class="fa fa-pencil"></i></button>
                          <button class="btn btn-danger btnEliminarPersonaFisica" idPersonaFisica="'.$value["id"].'"><i class="fa fa-times"></i></button>
                            ';

                      // if($_SESSION["tipousuario"] == 1){

                      //     echo '<button class="btn btn-danger btnEliminarPersonaFisica" idPersonaFisica="'.$value["id"].'"><i class="fa fa-times"></i></button>';

                      // }

                      echo '</div>  

                    </td>

                  </tr>';
          
            }

        ?>
   
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>



<!--=====================================
MODAL EDITAR CLIENTE
======================================-->

<div id="modalEditarPersonaFisica" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar cliente</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                <input type="text" class="form-control input-lg" name="editarPersonaFisica" id="editarPersonaFisica" required>
                <input type="hidden" id="idPersonaFisica" name="idPersonaFisica">
              </div>

            </div>

            <!-- ENTRADA PARA EL DOCUMENTO ID -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                <input type="number" min="0" class="form-control input-lg" name="editarDocumentoId" id="editarDocumentoId" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL EMAIL -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span> 

                <input type="correoelectronico" class="form-control input-lg" name="editarCorreoElectronico" id="editarCorreoElectronico" required>

              </div>

            </div>

            <!-- ENTRADA PARA EL TELÉFONO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="text" class="form-control input-lg" name="editarTelefono" id="editarTelefono" data-inputmask="'mask':'(999) 999-9999'" data-mask required>

              </div>

            </div>

            <!-- ENTRADA PARA LA DIRECCIÓN -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 

                <input type="text" class="form-control input-lg" name="editarDireccion" id="editarDireccion"  required>

              </div>

            </div>

             <!-- ENTRADA PARA LA FECHA DE NACIMIENTO -->
            
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 

                <input type="text" class="form-control input-lg" name="editarFechaNacimiento" id="editarFechaNacimiento"  data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>

              </div>

            </div>
  
          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

      </form>

      <?php

        $editarPersonaFisica = new ControladorPersonaFisica();
        $editarPersonaFisica -> ctrEditarPersonaFisica();

      ?>

    

    </div>

  </div>

</div>

<?php

  $eliminarPersonaFisica = new ControladorPersonaFisica();
  $eliminarPersonaFisica -> ctrEliminarPersonaFisica();

?>


