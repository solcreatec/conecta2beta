<?php

if($_SESSION["perfil"] == 3 || $_SESSION["perfil"] == 2){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

$tipopago = ControladorTipoPago::ctrMostrarTipoPago(null,null);
$sucursales = ControladorSucursal::ctrMostrarSucursales(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL); 
$usuarios = ControladorUsuarios::ctrMostrarUsuarios(NULL, NULL); 

foreach ($sucursales as $key => $value) { 
 
$contador = $key+1; 
}


// echo $_GET["reporte"].$_GET["fechaInicial"].$_GET["fechaFinal"].$_GET["sucursal"].$_GET["usuario"].$_GET["estatus"].$_GET["formapago"]; 
// exit();
?>

<input type="hidden" name="tipoReporte" id="tipoReporte" value="reporteventas">
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Reportes Ventas
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li>Reportes</li>
      
      <li class="active">Reportes Ventas</li>
    
    </ol>

  </section>

  <section class="content">

  <div class="box">
<!--INICIO DEL HEADER -->
          <div class="box-header with-border">

          <div class="form-group row">

            <div class="col-xs-2">

                 <div class="input-group">

                  <button type="button" class="btn btn-default" id="daterange-btn2">
                   
                    <span>
                      <i class="fa fa-calendar"></i> 

                      <?php

                        if(isset($_GET["fechaInicial"])){

                          echo $_GET["fechaInicial"]." - ".$_GET["fechaFinal"];
                        
                        }else{
                         
                          echo 'Rango de fecha';

                        }

                      ?>
                    </span>

                    <i class="fa fa-caret-down"></i>

                  </button>

               </div>

            </div>

            <div class="col-xs-2 box-tools pull-right">
                 <div class="input-group pull-right">

                  <button type="button" class="btn btn-default" id="agregarPago" data-toggle="collapse" data-target="#mostraropciones">
                  <i class="glyphicon glyphicon-plus"></i>
                  &nbsp;&nbsp;Agregar Opciones
                  </button>

                </div>
            </div>

          </div>

         </div>
<!-- FIN DEL HEADER -->

<!--=====================================
INICIO DE OPCIONES OCULTAS
======================================-->
 
<div id="mostraropciones"  class="box-footer with-border form-group collapse on">
  <div class="form-group row">

    <!-- ENTRADA PARA SELECCIONAR SUCURSAL --> 
    <div class="col-xs-2">

      <div class="input-group">

        <?php if ($contador > 1): ?> 

            <select class="form-control" id="sucursal" name="sucursal" required> 

            <option value="">Elige Sucursal</option> 

            <?php 

            foreach ($sucursales as $key => $value) { 

            echo '<option value="'.$value["id"].'">'.$value["nombre"].'</option>'; 
            } 

            ?> 

            </select> 

        <?php endif ?> 
      </div>

    </div>
    <!-- FIN --> 


  <div class="col-xs-2">

  <div class="input-group">

  <!-- ENTRADA PARA SELECCIONAR CATEGORÍA --> 

  <select class="form-control" id="usuario" name="usuario" required> 

  <option value="">Elige Usuario</option> 

  <?php 

  foreach ($usuarios as $key => $value) { 

  echo '<option value="'.$value["id"].'">'.$value["nombre"].'</option>'; 
  } 

  ?> 

  </select> 

  </div>

  </div>


  <div class="col-xs-2">

  <div class="input-group">

  <!-- ENTRADA PARA SELECCIONAR TIPO PAGO --> 
  <select class="form-control" name="formapago" id="formapago">
  <option value="">Elige Forma Pago</option>
  <?php 
  foreach ($tipopago as $key => $value) { 

  echo '<option value="'.$value['id'].'">'.$value['nombre'].'</option>';  } ?>
  </select>
  </div>

  </div>   

  <div class="col-xs-2">

  <div class="input-group">

  <!-- ENTRADA PARA SELECCIONAR CATEGORÍA --> 

  <select class="form-control" id="estatus" name="estatus" required> 

  <option value="">Elige Estatus</option> 

  <option value="1">Finalizado</option>
  <option value="0">Pendiente</option>

  </select> 
  </div>

  </div>   
  <div class="col-xs-2 box-tools pull-right">
  <div class="input-group pull-right">

  <?php

  if(isset($_GET["fechaInicial"])){

  echo '<a href="vistas/modulos/descargar-reporte.php?reporte=reporte&fechaInicial='.$_GET["fechaInicial"].'&fechaFinal='.$_GET["fechaFinal"].'&sucursal='.$_GET["sucursal"].'&usuario='.$_GET["usuario"].'&estatus='.$_GET["estatus"].'&formapago='.$_GET["formapago"].'">';

  }else{

  echo '<a href="vistas/modulos/descargar-reporte.php?reporte=reporte">';

  }         

  ?>

  <button class="btn btn-success" style="margin-top:5px">Descargar Excel</button>

  </a>

  </div>

  </div>   
  </div>

</div>
<!-- FIN DE OPCIONES OCULTAS -->

      <div class="row box-body">

          <div class="col-xs-12">
            
            <?php

            include "reportes/grafico-ventas.php";

            ?>

          </div>

           <div class="col-md-6 col-xs-12">
             
            <?php

            include "reportes/productos-mas-vendidos.php";

            ?>

           </div>

            <div class="col-md-6 col-xs-12">
             
            <?php

            include "reportes/vendedores.php";

            ?>

           </div>

           <div class="col-md-6 col-xs-12">
             
            <?php

            // include "reportes/compradores.php";

            ?>

           </div>
          

      </div>

</div>

</section>

</div>
