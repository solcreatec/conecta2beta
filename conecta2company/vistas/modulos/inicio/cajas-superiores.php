<?php

$item = null;
$valor = null;
$orden = "id";


$hoy = date('yy-m-d');
// echo $hoy; exit();

$ventahoy = ControladorVentas::ctrSumaVentasHoy($hoy, "act", 1, "sucursal_id", $_SESSION['sucursal_id'], NULL, NULL, NULL, NULL, NULL, NULL);
$ventasAplicadas = ControladorVentas::ctrSumaVentas("estatusventa", 1, "act", 1, "sucursal_id", $_SESSION['sucursal_id'], NULL, NULL, NULL, NULL);
$ventasPendientes = ControladorVentas::ctrSumaVentas("estatusventa", 0, "act", 1, "sucursal_id", $_SESSION['sucursal_id'], NULL, NULL, NULL, NULL);
$ventas = ControladorVentas::ctrSumaVentas("act", 1, "sucursal_id", $_SESSION['sucursal_id'], NULL, NULL, NULL, NULL, NULL, NULL);
?>

<div class="col-lg-3">

  <div class="small-box bg-aqua">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventahoy["total"],2); ?></h3>

      <p>Ganancias de Hoy</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventahoy["contador"]; ?></h3>

      <p>Ventas de Hoy</p>

    </div>
    
    <div class="icon">
      
      <i class="ion ion-ios-sunny"></i>
    
    </div>

    
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


<div class="col-lg-3">

  <div class="small-box bg-green">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventasAplicadas["total"],2); ?></h3>

      <p>Ganancias Cobradas</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventasAplicadas["contador"]; ?></h3>

      <p>Ventas Cobradas</p>

    </div>
    
        <div class="icon">
      
      <i class="ion-android-cart"></i>
    
    </div>
    
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


<div class="col-lg-3">

  <div class="small-box bg-yellow">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventasPendientes["total"],2); ?></h3>

      <p>Ganancias Pendientes</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventasPendientes["contador"]; ?></h3>

      <p>Ventas Pendientes</p>

    </div>
    
    <div class="icon">
      
      <i class="ion-ios-clock"></i>
    
    </div>
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>


<div class="col-lg-3">

  <div class="small-box bg-purple">
    
    <div class="inner">
      
      <h3>$<?php echo number_format($ventas["total"],2); ?></h3>

      <p>Ganancias Totales</p>
    
    </div>

    <div class="inner">

      <h3><?php echo $ventas["contador"]; ?></h3>

      <p>Ventas Generadas</p>

    </div>
    
    <div class="icon">
      
      <i class="ion ion-cash"></i>
    
    </div>
    
    <a href="ventas" class="small-box-footer">
      
      Más info <i class="fa fa-arrow-circle-right"></i>
    
    </a>

  </div>


</div>

