<?php
if($_SESSION["tipousuario_id"] == 3 || $_GET["idPersonaFisica"] == 0){

  echo '<script>

    window.location = "inicio";

  </script>';

  return;

}

$totalCorte = 0;
$totalEfectivo = 0;
$totalTD = 0;
$totalTC = 0;
$totalVales = 0;

$personafisica=ControladorPersonaFisica::ctrMostrarPersonaFisica("id",$_GET["idPersonaFisica"]);

// $personafisica = ControladorCorte::ctrMostrarCortes("id",$_GET["idPersonaFisica"],null,null,null,null,null,null,null,null);
$ventas = ControladorVentas::ctrVentasMostrar("corte_id", $_GET["idPersonaFisica"],null,null,null,null,null,null,null,null);
$sucursal = ControladorSucursal::ctrMostrarSucursales("id",$personafisica["sucursal_id"], NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
// echo $sucursal["nombre"]; exit();


$cajero = ControladorUsuarios::ctrMostrarUsuarios("id", $personafisica["usuario_id"]);



?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1> Detalle Contacto</h1>

    <ol class="breadcrumb">
      
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li><a href="personasfisicas">Personas Fisicas</a></li>
      
      <li class="active">Detalle Corte</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <div class="callout callout-info">
            <h4 >Agregar contacto</h4><h5> Recuerda que al crear este contacto solo será para servicios residenciales.</h5>
        </div>
      </div>

      <div class="box-body">
          <!-- <div class="col-md-8"> -->

            <form role="form" method="post">

             <!-- ENTRADA PARA NOMBRE COMPLETO  -->
              <div class="col-md-4 form-group">
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevaPersonaFisica" placeholder="Escribe nombre" value="<?php echo $personafisica["nombre"]; ?>" readonly>

                  </div>
              </div>

              <div class="col-md-4 form-group">
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoApellidoPaterno" placeholder="Escribe apellido paterno" value="<?php echo $personafisica["apellidopaterno"]; ?>" readonly>

                  </div>
              </div>

              <div class="col-md-4 form-group">
                  <div class="input-group">
                  
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control input-lg" name="nuevoApellidoMaterno" placeholder="Escribe apellido materno" value="<?php echo $personafisica["apellidomaterno"]; ?>" readonly>

                  </div>
              </div>


             <!-- ENTRADA PARA LA FECHA DE NACIMIENTO -->
            
            <div class="col-md-4 form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevaFechaNacimiento" placeholder="Escribe la fecha denacimiento" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask value="<?php echo $personafisica["fechanacimiento"]; ?>" readonly>


              </div>

            </div>

            <!-- ENTRADA PARA EL CELULAR -->
            
            <div class="col-md-4 form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevoCelular" placeholder="Escriber celular" data-inputmask="'mask':'(999) 999-9999'" data-mask value="<?php echo $personafisica["celular"]; ?>" readonly>

              </div>

            </div>

            <!-- ENTRADA PARA EL TELÉFONO -->
            
            <div class="col-md-4 form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 

                <input type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Escriber teléfono" data-inputmask="'mask':'(999) 999-9999'" data-mask value="<?php echo $personafisica["telefono"]; ?>" readonly>

              </div>

            </div>
            
             <!-- ENTRADA PARA EL EMAIL -->
            
            <div class="col-md-8 form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span> 

                <input type="correoelectronico" class="form-control input-lg" name="nuevoCorreoElectronico" placeholder="Escribe el correo electronico" value="<?php echo $personafisica["correoelectronico"]; ?>" readonly>

              </div>

            </div>
            <!-- ENTRADA PARA EL RFC -->
            
            <div class="col-md-4 form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-key"></i></span> 

                <input minlength="12" maxlength="13" class="form-control input-lg" name="nuevoRFC" placeholder="Escribe RFC" value="<?php echo $personafisica["rfc"]; ?>" readonly>

              </div>

            </div>


<!-- <div class="col-md-4"></div> -->

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="col-md-12 modal-footer">

          <!-- <button type="button" class="btn btn-default pull-left" >Atras</button> -->
                <button type="submit" class="btn btn-primary">Guardar cliente</button>
          </form>

          <a href="personasfisicas">
             <button type="button" class="btn btn-light pull-left">
                  <i class="glyphicon glyphicon-share-alt"></i>&nbsp;&nbsp;Regresar
              </button>
          </a>
        </div>



      <?php

        $crearPersonaFisica = new ControladorPersonaFisica();
        $crearPersonaFisica -> ctrCrearPersonaFisica();

      ?>



      </div>

    </div>

  </section>

</div>

<?php

  // $eliminarPersonaFisica = new ControladorPersonaFisica();
  // $eliminarPersonaFisica -> ctrEliminarPersonaFisica();

?>