<?php 
$item = null;
$tipopago = ControladorTipoPago::ctrMostrarTipoPago($item,$value);

$venta = ControladorVentas::ctrMostrarVentas("id", $_GET["idVenta"]);
$vendedor = ControladorUsuarios::ctrMostrarUsuarios("id", $venta["usuario_id"]);      
$pagos = ControladorPago::ctrMostrarPagos("venta_id", $_GET["idVenta"]);
    
$sumaPagos = ControladorPago::ctrSumaPago("venta_id", $_GET["idVenta"]);
// echo $sumaPagos['total']; exit();
$pendiente = $venta['total'] - $sumaPagos['total']; 
// echo $pendiente; exit();
?>

<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Editar Venta 
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li><a href="ventas">Ventas</a></li>
      
      <li class="active">Editar Venta</li>
    
    </ol>

  </section>

  <section class="content">

<!-- ALERTAS DE INFORMACIÓN DE LA VENTA -->

  <?php if ($venta['estatusventa']==1) { ?>
      <p class="alert alert-success">
      <i class="glyphicon glyphicon-ok-sign"></i> 
      <B>&nbsp;&nbsp;Esta venta esta COMPLETADA.</B>
      </p>
  <?php } else{ ?>
      <p class="alert alert-warning">
      <i class="glyphicon glyphicon-time">
      </i><B>&nbsp;&nbsp;Esta venta esta PENDIENTE, agrega un pago para finalizarla.</B></p>
  <?php }?>

    <div class="row">

      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-5 col-xs-12">
        
        <div class="box box-success">
          
          <div class="box-header with-border"></div>

          <form role="form" method="post" class="formularioVenta">

            <div class="box-body">
  
              <div class="box">


                <!--=====================================
                ENTRADA DEL VENDEDOR
                ======================================-->
            
                <div class="form-group">
                
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control" id="nuevoVendedor" value="<?php echo $vendedor["nombre"]; ?>" readonly>

                    <input type="hidden" name="idVendedor" value="<?php echo $vendedor["id"]; ?>">

                  </div>

                </div> 

                <!--=====================================
                ENTRADA DEL CÓDIGO
                ======================================--> 

                <div class="form-group">
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>

                   <input type="text" class="form-control" id="nuevaVenta" name="editarVenta" value="<?php echo $venta["codigo"]; ?>" readonly>
               
                  </div>
                
                </div>

                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

                <div class="form-group row nuevoProducto">

                <?php

                $listaProducto = json_decode($venta["productos"], true);

                foreach ($listaProducto as $key => $value) {

                  $respuesta = ControladorProductos::ctrMostrarProductos("id", $value["id"], "id");

                  $stockAntiguo = $respuesta["stock"] + $value["cantidad"];
                  
                  echo '<div class="row" style="padding:5px 15px">
            
                        <div class="col-xs-6" style="padding-right:0px">
            
                          <div class="input-group">
                
                            <span class="input-group-addon"><button type="button" class="btn btn-danger btn-xs quitarProducto" idProducto="'.$value["id"].'"><i class="fa fa-times"></i></button></span>

                            <input type="text" class="form-control nuevaDescripcionProducto" idProducto="'.$value["id"].'" name="agregarProducto" value="'.$value["descripcion"].'" readonly required>

                          </div>

                        </div>

                        <div class="col-xs-3">
              
                          <input type="number" class="form-control nuevaCantidadProducto" name="nuevaCantidadProducto" min="1" value="'.$value["cantidad"].'" stock="'.$stockAntiguo.'" nuevoStock="'.$value["stock"].'" required>

                        </div>

                        <div class="col-xs-3 ingresoPrecio" style="padding-left:0px">

                          <div class="input-group">

                            <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>
                   
                            <input type="text" class="form-control nuevoPrecioProducto" precioReal="'.$respuesta["precioventa"].'" name="nuevoPrecioProducto" value="'.$value["total"].'" readonly required>
   
                          </div>
               
                        </div>

                      </div>';
                }


                ?>

                </div>

                <input type="hidden" id="listaProductos" name="listaProductos">

                <!--=====================================
                BOTÓN PARA AGREGAR PRODUCTO
                ======================================-->

                <button type="button" class="btn btn-default hidden-lg btnAgregarProducto">Agregar producto</button>

                <hr>

                <div class="row">

                  <!--=====================================
                  ENTRADA IMPUESTOS Y TOTAL
                  ======================================-->
                  
                  <div class="col-xs-8 pull-right">
                    
                    <table class="table">

                      <thead>

                        <tr>
                          <th>Total</th>      
                        </tr>

                      </thead>

                      <tbody>
                      
                        <tr>
                           
                        <input type="hidden" class="form-control input-lg" min="0" id="nuevoImpuestoVenta" name="nuevoImpuestoVenta" value="<?php echo $porcentajeImpuesto; ?>" required>

                         <input type="hidden" name="nuevoPrecioImpuesto" id="nuevoPrecioImpuesto" value="<?php echo $venta["impuesto"]; ?>" required>

                         <input type="hidden" name="nuevoPrecioNeto" id="nuevoPrecioNeto" value="<?php echo $venta["neto"]; ?>" required>

                         <input type="hidden" name="idVenta" id="idVenta" value="<?php echo $venta["id"]; ?>" required>
                        

                           <td style="width: 100%">
                            
                            <div class="input-group">
                           
                              <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                              <input type="text" class="form-control" id="nuevoTotalVenta" name="nuevoTotalVenta" total="<?php echo $venta["neto"]; ?>"  value="<?php echo $venta["total"]; ?>" readonly required>

                              <input type="hidden" name="totalVenta" value="<?php echo $venta["total"]; ?>" id="totalVenta">
                              
                        
                            </div>

                            <div class="box-body table-responsive" id="cambiovisualdiv">   

                                <div class="form-group" >
                                  <label class="col-lg-6 control-label">Pago</label>
                                  <div class="col-md-12 input-group">

                                    <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>
                                    <input type="text" id="pagovisual" class="form-control" required readonly>
                                    <input type="hidden" id="pagoventa" name="pagoventa" class="form-control" required value="0" >
                                  </div>
                                </div>

                                <div class="form-group" >
                                  <label class="col-lg-6 control-label">Cambio</label>
                                  <div class="col-md-12 input-group">

                                    <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>
                                    <input type="text" id="cambiovisual" class="form-control" required readonly>
                                    <input type="hidden" id="cambioventa" name="cambioventa" class="form-control" required value="0" >
                                  </div>
                                </div>

                            </div>

                          </td>

                        </tr>

                      </tbody>

                    </table>
<?php if ($pagos != null){  ?>
            <table class="table">

              <thead>

                <tr>
                <th>Pagado</th>      
                </tr>

              </thead>

              <tbody>

                <tr>

                  <td style="width: 90%">

                  <div class="input-group">

                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" value="<?php echo number_format($sumaPagos["total"],2); ?>" readonly required>

                  </div>

                  </td>

                </tr>

              </tbody>

            </table>
<?php } 

if ($pendiente >0) {?>
            <table class="table">

              <thead>

                <tr>
                <th>Pendiente</th>      
                </tr>

              </thead>

              <tbody>

                <tr>

                  <td style="width: 90%">

                  <div class="input-group">

                  <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                  <input type="text" class="form-control" id="totalPendiente" name="totalPendiente" value="<?php echo number_format($pendiente,2); ?>" readonly required>

                  </div>

                  </td>

                </tr>

              </tbody>

            </table>
<?php } ?>
                  </div>

                </div>

    <table class="table">
      <thead>
        <tr>
          <th>Comentarios</th>
        </tr>
      </thead>

      <tbody>
        <tr>
           <td>
            <div class="input-group">

                <?php if ($venta['comentario'] != NULL) {
                  echo '<textarea name="comentarios-sm" class="form-control" id="comentarios-sm" name="comentarios-sm">'.$venta['comentario'].'
                  </textarea>'; 

                } else{

                  echo '<textarea name="comentarios-sm" class="form-control" id="comentarios-sm" name="comentarios-sm" placeholder="Escribe tus comentarios adicionales sobre esta venta..."></textarea>';
                }?>
              

            </div>
          </td>
        </tr>
      </tbody>
    </table>
              </div>

<div id="agregarPagoDiv">
        <button type="button" class="btn btn-default" id="agregarPago" data-toggle="collapse" 
      data-target="#mostrartipopago">
      <i class="glyphicon glyphicon-plus"></i>
      &nbsp;&nbsp;Agregar Pago
      </button>
</div>


          </div>

          <div class="box-body">

                <!--=====================================
                ENTRADA MÉTODO DE PAGO
                ======================================-->
 
                <div id="mostrartipopago"  class="form-group row collapse on">

                   <div class="col-xs-6">

                     <div class="input-group">

                      <select class="form-control" name="nuevoMetodoPago" id="nuevoMetodoPago">
                        <option value=""></option>
                                  <?php 
                                foreach ($tipopago as $key => $value) { 
                                  
                                  echo '<option value="'.$value['id'].'">'.$value['nombre'].'</option>';  } ?>
                      </select>

                    </div>
                  </div>

                   <div class="col-xs-6">

                      <div class="input-group" id="cantidadpagodiv">
                        <input type="number" id="cantidadpago" name="cantidadpago" class="form-control" placeholder="Cantidad a Pagar...." >
                    </div>

                      <div class="input-group" id="idtransacciondiv">
                        <input type="number" id="idtransaccion" name="idtransaccion" class="form-control" placeholder="ID Transaccion...." >
                    </div>
                  </div>
                </div>
          </div>

          <div class="box-footer">

            <a href="ventas"class="btn btn-danger pull-right">
         <i class="fa fa-times" aria-hidden="true"></i>

      Cancelar
        </a>

            <button type="submit" class="btn btn-primary pull-left">

            <i class="fa fa-refresh"></i>
            &nbsp;&nbsp;Actualizar

            </button>

          </div>

        </form>

        <?php

          $editarVenta = new ControladorVentas();
          $editarVenta -> ctrEditarVenta();
          
        ?>

        </div>
            
      </div>

      <!--=====================================
      LA TABLA DE PRODUCTOS
      ======================================-->

      <div class="col-lg-7 col-xs-12">
        
        <div class="box box-warning">

          <div class="box-header with-border"></div>

          <div class="box-body">
            
            <table class="table table-bordered table-striped dt-responsive tablaVentas">
              
               <thead>
                 <tr>
                  <th >#</th>
                  <th >Código</th>
                  <th>Descripcion</th>
                  <th>Stock</th>
                  <th>Acciones</th>
                  <th>Imagen</th>
                </tr>
              </thead>

            </table>

           <input type="hidden" value="<?php echo $_SESSION['tipousuario_id']; ?>" id="tipoUsuarioOculto">
           <input type="hidden" value="<?php echo $_SESSION['sucursal_id']; ?>" id="sucursalOculto">
           
          </div>

        </div>

<?php if ($pagos != null){ ?>
  
        <div class="box box-info">

          <div class="box-header with-border"><h4><B>
            <i class="fa fa-list" aria-hidden="true"></i>
                &nbsp;Lista de Pagos</B></h4></div>

            <div class="box-body">
            
                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

          
       <table class="table table-bordered table-striped dt-responsive" width="100%">
         
        <thead>
         
         <tr>
           
           <th style="width:10px">#</th>
           <th> Fecha</th>
           <th> Cajero</th>
           <th> Tipo Pago</th>
           <th> Pago</th> 
           <th> Comentario</th>
           <th> Pagado</th>

         </tr> 

        </thead>
                <?php

                foreach ($pagos as $key => $value) {

                  $tipopago = ControladorTipoPago::ctrMostrarTipoPago("id",$value["tipopago_id"]); 
                  $cajero = ControladorUsuarios::ctrMostrarUsuarios("id", $value["usuario_id"]);

                  echo '<tr>
                  <td>'.($key+1).'</td>
                  <td>'.$value["fechaalta"].'</td>
                  <td>'.$cajero["nombre"].'</td>
                  <td>'.$tipopago["nombre"].'</td>
                  <td>$ '.number_format($value["cantidad"],2).'</td>';

                  if ($value['tipopago_id'] == 1) {

                    echo '<td> SU CAMBIO FUE <b>$'.$value["datooperacion"].'</b></td>';

                  }else if ($value['tipopago_id'] == 2 || $value['tipopago_id'] == 3){

                    echo '<td>ID TRANSACCION: <b>'.$value["datooperacion"].'</b></td>';

                  } else{

                    echo '<td><b>'.$value["datooperacion"].'</b></td>';
                  }

                  
                  echo '<td><B>$ '.number_format($value["total"],2).'</td>
                  </tr>';
         }?>
</table>
</div>
</div>
<?php } ?>
      </div>


    </div>
   
  </section>

</div>

