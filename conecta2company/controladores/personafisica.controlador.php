<?php

class ControladorPersonaFisica{

	/*=============================================
	CREAR PERSONA FISICA
	=============================================*/

	static public function ctrCrearPersonaFisica(){

		if(isset($_POST["nuevaPersonaFisica"])){

		$tel=strlen($_POST["nuevoTelefono"]);
		$am=strlen($_POST["nuevoApellidoMaterno"]);
		$hoy= date("d-m-Y");
		// $fechanacimiento=date('d-m-Y',strtotime($_POST['nuevaFechaNacimiento']));
		$edad = date_diff(date_create($_POST['nuevaFechaNacimiento']), date_create($hoy));
		// echo 'Mi edad es: '.$edad->format('%y')."==".$_POST['nuevaFechaNacimiento']; exit();

			if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaPersonaFisica"]) ){

				if(preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoApellidoPaterno"]) ){

						if(preg_match('/^[()\-0-9 ]+$/', $_POST["nuevoCelular"]) ){

							if(preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["nuevoCorreoElectronico"])){


									if($am==0 || preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+$/', $_POST["nuevoApellidoMaterno"])){
										
										if($tel==0 || preg_match('/^[()\-0-9 ]+$/', $_POST["nuevoTelefono"]) ){

											if ($edad->format('%y')>=18) {
												
												$tabla = "personafisica";

												$datos = array(
												"nombre"=>$_POST["nuevaPersonaFisica"],
												"apellidopaterno"=>$_POST["nuevoApellidoPaterno"],
												"apellidomaterno"=>$_POST["nuevoApellidoMaterno"],
												"rfc"=>$_POST["nuevoRFC"],
												"correoelectronico"=>$_POST["nuevoCorreoElectronico"],
												"telefono"=>$_POST["nuevoTelefono"],
												"celular"=>$_POST["nuevoCelular"],
												"fechanacimiento"=>$_POST["nuevaFechaNacimiento"]
												);

												$respuesta = ModeloPersonaFisica::mdlIngresarPersonaFisica($tabla, $datos);


												if($respuesta == "ok"){

												echo'<script>

												swal({
												type: "success",
												title: "El cliente ha sido guardado correctamente",
												showConfirmButton: true,
												confirmButtonText: "Cerrar"
												}).then(function(result){
												if (result.value) {

												window.location = "personasfisicas";

												}
												})

												</script>';

												}

											}else{

										echo'<script>

										swal({
										type: "error",
										title: "¡Esta persona no es mayor de edad!",
										showConfirmButton: true,
										confirmButtonText: "Cerrar"
										}).then(function(result){
										if (result.value) {

										window.location = "#";

										}
										})

										</script>';

											}



											}else{

										echo'<script>

										swal({
										type: "error",
										title: "¡Numero de telefono incorrecto, faltan digitos!",
										showConfirmButton: true,
										confirmButtonText: "Cerrar"
										}).then(function(result){
										if (result.value) {

										window.location = "#";

										}
										})

										</script>';

											}
									}else{


										echo'<script>

										swal({
										type: "error",
										title: "¡Escribiste caracteres no permitidos en Apellido Materno!",
										showConfirmButton: true,
										confirmButtonText: "Cerrar"
										}).then(function(result){
										if (result.value) {

										window.location = "#";

										}
										})

										</script>';

									}



						}else{

							echo'<script>

							swal({
							type: "error",
							title: "¡Correo escrito de manera incorrecta!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
							}).then(function(result){

									
							if (result.value) {

							window.location = "#";

							}
							})

							</script>';

					    }

						}else{

							echo'<script>

							swal({
							type: "error",
							title: "¡Numero de celular incorrecto, faltan digitos!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
							}).then(function(result){
							if (result.value) {

							window.location = "#";

							}
							})

							</script>';

					}


				}else{

					echo'<script>

						swal({
							  type: "error",
							  title: "¡Escribiste caracteres no permitidos en Apellido Paterno!",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
								if (result.value) {

								window.location = "#";

								}
							})

				  	</script>';

				}
			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡Escribiste caracteres no permitidos en el nombre!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "#";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	MOSTRAR PERSONAS FISICAS
	=============================================*/

	static public function ctrMostrarPersonaFisica($item, $valor){

		$tabla = "personafisica";

		$respuesta = ModeloPersonaFisica::mdlMostrarPersonaFisica($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	EDITAR PERSONA FISICA
	=============================================*/

	static public function ctrEditarPersonaFisica(){

		if(isset($_POST["editarPersonaFisica"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarPersonaFisica"]) &&
			   preg_match('/^[0-9]+$/', $_POST["editarDocumentoId"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["editarEmail"]) && 
			   preg_match('/^[()\-0-9 ]+$/', $_POST["editarTelefono"]) && 
			   preg_match('/^[#\.\-a-zA-Z0-9 ]+$/', $_POST["editarDireccion"])){

			   	$tabla = "personafisica";

			   	$datos = array("id"=>$_POST["idPersonaFisica"],
			   				   "nombre"=>$_POST["editarPersonaFisica"],
					           "documento"=>$_POST["editarDocumentoId"],
					           "correoelectronico"=>$_POST["editarEmail"],
					           "telefono"=>$_POST["editarTelefono"],
					           "direccion"=>$_POST["editarDireccion"],
					           "fechanacimiento"=>$_POST["editarFechaNacimiento"]);

			   	$respuesta = ModeloPersonaFisicas::mdlEditarPersonaFisica($tabla, $datos);

			   	if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El contacto ha sido cambiado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "PersonaFisicas";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El contacto no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "PersonaFisicas";

							}
						})

			  	</script>';



			}

		}

	}

	/*=============================================
	ELIMINAR PERSONA FISICA
	=============================================*/

	static public function ctrEliminarPersonaFisica(){

		if(isset($_GET["idPersonaFisica"])){

			$tabla ="PersonaFisicas";
			$datos = $_GET["idPersonaFisica"];

			$respuesta = ModeloPersonaFisicas::mdlEliminarPersonaFisica($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El contacto ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result){
								if (result.value) {

								window.location = "PersonaFisicas";

								}
							})

				</script>';

			}		

		}

	}

}

