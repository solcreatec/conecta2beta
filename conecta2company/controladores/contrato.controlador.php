<?php


class ControladorContrato{

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/

	static public function ctrMostrarContratos($item, $valor){

		$tabla = "contratos";

		$respuesta = ModeloContrato::mdlMostrarContratos($tabla, $item, $valor);

		return $respuesta;

	}


	static public function ctrContratosMostrar($item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5){

		$tabla = "contratos";

		$respuesta = ModeloContrato::mdlContratosMostrar($tabla, $item, $valor, $item2, $valor2, $item3, $valor3, $item4, $valor4, $item5, $valor5);

		return $respuesta;
		
	}



	/*=============================================
	RANGO FECHAS
	=============================================*/	

	static public function ctrRangoFechasContrato($fechaInicial, $fechaFinal){

		$tabla = "contratos";

		$respuesta = ModeloContrato::mdlRangoFechasContrato($tabla, $fechaInicial, $fechaFinal);

		return $respuesta;
		
	}


	/*=============================================
	CREAR VENTA
	=============================================*/

	static public function ctrCrearContrato(){

		if(isset($_POST["nuevoContrato"])){

			/*=============================================
			ACTUALIZAMOS USUARIO
			=============================================*/	

			$item = "id";
			$valor = $_SESSION["id"];
			$item1b = "ultimo_movimiento";
			$fecha = date('Y-m-d');
			$hora = date('H:i:s');
			$valor1b = $fecha.' '.$hora;

			$fechaUsuario = ControladorUsuarios::ctrActualizarUsuario($item1b, $valor1b, $item, $valor);

			/*=============================================
			GUARDAR Contrato
			=============================================*/	

			$tabla = "contratos";

			$datos = array("usuario_id"=>$valor,
						   "codigo"=>$_POST["nuevoContrato"],
						   "total"=>$_POST["nuevoTotalContrato"],
						   "fechaalta"=>$valor1b,
						   "sucursal_id"=>$_SESSION["sucursal_id"]);

			$ingresa = ModeloContrato::mdlIngresarContrato($tabla, $datos);
			// echo $respuesta;
				$item2 = "usuario_id";
				$max = ModeloContrato::mdlMaxIDContrato($tabla, "usuario_id", $valor);
				// echo "NUM: ".$max['idmax']; exit();

				$nuevoContrato = ModeloContrato::mdlMostrarContratos($tabla, $item, $max['idmax'], null, null, null, null, null, null, null, null);

				$tablaVentas = "ventas";

				if ($_SESSION["tipousuario_id"]==1) {
					$respuesta = ControladorVentas::ctrActualizarVenta("Contrato_id", $max['idmax'], "Contrato_id", "0", null, null,NULL,NULL,NULL,NULL,NULL,NULL);
				}else{
					$respuesta = ControladorVentas::ctrActualizarVenta("Contrato_id", $max['idmax'], "Contrato_id", "0", "usuario_id", $valor,"sucursal_id",$_SESSION["sucursal_id"],NULL,NULL,NULL,NULL);
				}


			if($respuesta == "ok"){
				
				echo'<script>

				localStorage.removeItem("rango");

				swal({
					  type: "success",
					  title: "El Contrato '.$nuevoContrato['codigo'].' ha sido guardado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "Contratocaja";

								}
							})

				</script>';

			}else{
								echo'<script>

				localStorage.removeItem("rango");

				swal({
					  type: "error",
					  title: "El Contrato no ha podido generarse",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result){
								if (result.value) {

								window.location = "#";

								}
							})

				</script>';
			}

		}

	}

static public function ctrMaxIDContrato($item, $valor){

		$tabla = "contratos";

		$respuesta = ModeloContrato::mdlMaxIDContrato($tabla, $item, $valor);

		return $respuesta;

}
}