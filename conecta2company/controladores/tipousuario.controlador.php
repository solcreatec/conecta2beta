<?php

class ControladorTipoUsuario{

	/*=============================================
	CREAR CATEGORIAS
	=============================================*/

	static public function ctrCrearTipoUsuario(){

		if(isset($_POST["nuevaTipoUsuario"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaTipoUsuario"])){

				$tabla = "tipousuario";

				$datos = $_POST["nuevaTipoUsuario"];

				$respuesta = ModeloTipoUsuario::mdlIngresarTipoUsuario($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido guardada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "tipousuario";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "tipousuario";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	MOSTRAR CATEGORIAS
	=============================================*/

	static public function ctrMostrarTipoUsuario($item, $valor){

		$tabla = "tipousuario";

		$respuesta = ModeloTipoUsuario::mdlMostrarTipoUsuario($tabla, $item, $valor);

		return $respuesta;
	
	}

	/*=============================================
	EDITAR CATEGORIA
	=============================================*/

	static public function ctrEditarTipoUsuario(){

		if(isset($_POST["editarTipoUsuario"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarTipoUsuario"])){

				$tabla = "tipousuario";

				$datos = array("categoria"=>$_POST["editarTipoUsuario"],
							   "id"=>$_POST["idTipoUsuario"]);

				$respuesta = ModeloTipoUsuario::mdlEditarTipoUsuario($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido cambiada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "tipousuario";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "tipousuario";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	BORRAR CATEGORIA
	=============================================*/

	static public function ctrBorrarTipoUsuario(){

		if(isset($_GET["idTipoUsuario"])){

			$tabla ="TipoUsuario";
			$datos = $_GET["idTipoUsuario"];

			$respuesta = ModeloTipoUsuario::mdlBorrarTipoUsuario($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido borrada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "tipousuario";

									}
								})

					</script>';
			}
		}
		
	}
}
